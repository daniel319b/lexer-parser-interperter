﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Collections;

namespace Lexer
{
    class ParseRule
    {
        
        //exp : exp PLUS exp
        //exp : exp MINUS exp
        //exp : NUMBER
        public string Rule { get; set; }

        public List<object> elements;

        public ParseRule(string rule)
        {
            Rule = rule;
            Regex r = new Regex("[a-zA-Z]+");
            var matches = r.Matches(rule);

            elements = new List<object>() { matches[0].Value};
            List<string> a = new List<string>();
            for (int i = 1; i < matches.Count; i++)
                a.Add(matches[i].Value);
            elements.Add(a);
        }

       

    }
}
