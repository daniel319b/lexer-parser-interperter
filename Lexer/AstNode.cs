﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lexer;

namespace Lexer
{
    public enum NodeType
    {
        Add,
        Subtract,
        Multiply,
        Divide,
        Number,
        Assign,
        Identifier,
        Equals,
        Smaller,
        Bigger,
        BiggerOrEqual,
        SmallerOrEqual,
        Or,
        And,
        Print,
        String
    }

    public class AstNode
    {
        public NodeType NodeType { get; set; }

        public bool IsExpression { get; set; }

        public bool IsLeaf { get; set; }

        public AstNode(NodeType nodeType)
        {
            NodeType = nodeType;
            IsLeaf = false;
        }

        protected AstNode()
        {
            
        }
    }

   

    public class Number : AstNode
    {
        public object Value { get; set; }

        public Number (object value) : base (NodeType.Number)
        {
            Value = value;
            IsLeaf = true;
        }

        public override string ToString()
        {
            return string.Format("(number,{0})", Value);
        }
    }

    public class Identifier : AstNode
    {
        public string Value { get; set; }
        
        public Identifier(string name):base(NodeType.Identifier)
        {
            Value = name;
        }

        public override string ToString()
        {
            return string.Format("({0},{1})",NodeType,Value);
        }
    }

    public class BinaryExpression : AstNode
    {
        public AstNode Operand1 { get; set; }
        public AstNode Operand2 { get; set; }
       
        public BinaryExpression(AstNode operand1,AstNode operand2,string op)
        {
            Operand1 = operand1;
            Operand2 = operand2;
            IsExpression = true;
            if (op == "+")
                NodeType = NodeType.Add;
            else if (op == "-")
                NodeType = NodeType.Subtract;
            else if (op == "*")
                NodeType = NodeType.Multiply;
            else if (op == "/")
                NodeType = NodeType.Divide;
            else if (op == "=")
                NodeType = NodeType.Equals;
            else if (op == "<")
                NodeType = NodeType.Smaller;
            else if (op == ">")
                NodeType = NodeType.Bigger;
            else if (op == ">=")
                NodeType = NodeType.BiggerOrEqual;
            else if (op == "<=")
                NodeType = NodeType.SmallerOrEqual;
            else if (op == "||")
                NodeType = NodeType.Or;
            else if (op == "&&")
                NodeType = NodeType.And;
            else
                throw new Exception(string.Format("Operator {0} is not legal!", op));
            

        }

        public override string ToString()
        {
            return string.Format("({0},{1},{2})",NodeType,Operand1,Operand2);
        }

        
    }

    public class AssignmentStatment : AstNode
    {
        public string Identifier { get; set; }
        public AstNode Expression { get; set; }

        public AssignmentStatment(string identifier,AstNode exp):base(NodeType.Assign)
        {
            Identifier = identifier;
            Expression = exp;
        }

        public override string ToString()
        {
            return string.Format("({0},{1},{2})",NodeType,Identifier,Expression);
        }
    }

    public class PrintStatement : AstNode
    {
        public AstNode ValueTree { get; set; }

        public PrintStatement(AstNode treeValue):base(NodeType.Print)
        {
            ValueTree = treeValue;
        }

        public override string ToString()
        {
            return string.Format("({0},{1})",NodeType,ValueTree);
        }
    }

    public class StringNode : AstNode
    {
        public string Value { get; set; }

        public StringNode(string value): base(NodeType.String)
        {
            Value = value;
        }

        public override string ToString()
        {
            return string.Format("({0},{1})",NodeType,Value);
        }
    }
}
