﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Lexer
{
    class Parser
    {
        List<Token> tokens;
        Grammmar grammar = new Grammmar();
        List<Node> tree = new List<Node> ();

        public Parser(List<Token> tokens)
        {
            this.tokens = tokens;
        }

        public AstNode Parse()
        {
           // tree = new List<string>();
           // tree.Add("5+5");
            tokens.Add(new Token("end", "END"));
            Dictionary<int, List<ParseState>> chart = new Dictionary<int, List<ParseState>>();
            for (int i = 0; i < tokens.Count + 1; i++)
                chart[i] = new List<ParseState>();
            ParseRule start_rule = grammar.Rules[0];
            ParseState start_state = new ParseState(start_rule.elements[0], new List<string>() { }, (List<string>)start_rule.elements[1], 0);
            chart[0] = new List<ParseState>() { start_state };

            for (int i = 0; i < tokens.Count; i++)
            {
                while (true)
                {
                    bool changes = false;

                    //foreach (ParseState state in chart[i])
                    for (int k = 0; k < chart[i].Count; k++)
                    { 
                        //State ===   x -> a b . c d , j
                        ParseState state = chart[i][k];
                        string x = state.x;
                        List<string> ab = state.ab;
                        List<string> cd = state.cd;
                        int j = (int)state.j;

                        List<ParseState> next_states = closure(grammar, i, x, ab, cd);
                        foreach (ParseState next_state in next_states)                   
                            changes = AddToChart(chart, i, next_state) || changes;                           

                        ParseState nextState = Shift(tokens, i, x, ab, cd, j,state);
                        if (nextState != null)
                            changes = AddToChart(chart, i + 1, nextState) || changes;
                        
                        next_states = Reductions(chart, i, x, ab, cd, j,state);
                        foreach (ParseState next_state in next_states)
                            changes = AddToChart(chart, i, next_state) || changes;

                       // if (!changes) break;
                    }
                    if (changes) break;
                }
            }

            Console.WriteLine();
            for (int i = 0; i < tokens.Count; i++)
			{ 
                Console.WriteLine("== chart " +i);
                foreach(ParseState state in chart[i])
                 // if(state.Completed)
                //  if(state.j == 0 && state.Completed)
                      Console.WriteLine(state);
			}

           
            ParseState acceptingState = new ParseState (start_rule.elements[0],(List<string>)start_rule.elements[1],new List<string>(),0);
           // AstNode parsedTree = ParseExpression(tokens);
            
            bool isParsed = chart[tokens.Count - 1].Contains(acceptingState);


            ParseState finishState = chart[tokens.Count - 1].Find(state => state.Completed && state.j == 0);
            AstNode parseTree = finishState.tree[0];
           // return isParsed;
            return parseTree;
        }




        #region Parsing Algorithm

       
 
        private List<ParseState> Reductions(Dictionary<int, List<ParseState>> chart, int i, string x, List<string> ab,
                                            List<string> cd, int j,ParseState state)
        {
            List<ParseState> res = new List<ParseState>();
            foreach (ParseState jstate in chart[j])
            {
                //(jstate[0], jstate[1]+[x], (jstate[2])[1:], jstate[3])
                //jstate is the state that we need to return to.
                //jstate = x-> ab.Cd(C non terminal) | and this state = C->abcd.
                //jstate = exp->.exp+exp | this state = exp->NUMBER.
                if (cd.Count == 0 && jstate.cd.Count > 0 && jstate.cd[0] == x)
                {
                    List<string> newAB = new List<string>(jstate.ab);
                    newAB.AddRange(new List<string>() {x}); //ab x
                    List<string> newCD = new List<string>(jstate.cd);
                    newCD.RemoveAt(0);

                    ParseState NewState = new ParseState(jstate.x, newAB, newCD, jstate.j);
                    foreach(AstNode s in jstate.abTree)
                        NewState.abTree.Add(s);
                    foreach(AstNode s in state.tree)
                         NewState.abTree.Add(s);

                    if (NewState.Completed)
                    {
                        //string s ="";
                     //   if(NewState.abTree.Count > 2)
                         //  s = ((Number)NewState.abTree[1]).Value.ToString();

                        //if (s == "*" || s == "/" || s=="+" || s == "-")
                        if ((NewState.x == "Expression" || NewState.x == "Term" || NewState.x == "BoolExpression") && NewState.abTree.Count > 1)
                        {
                            string s = ((Number)NewState.abTree[1]).Value.ToString();
                            NewState.tree.Add(new BinaryExpression(NewState.abTree[0], NewState.abTree[2], s));
                        }
                        else if (NewState.x == "Assignment")
                            NewState.tree.Add(new AssignmentStatment(((Identifier)NewState.abTree[0]).Value, NewState.abTree[2]));
                        else if (NewState.x == "PrintStatement")
                        {
                            // {(identifier,print)} {(Add,(number,1),(number,3))}
                            NewState.tree.Add(new PrintStatement(NewState.abTree[1]));
                        }
                         

                        else
                            NewState.tree = NewState.abTree;
                    }
                    res.Add(NewState);
                   
                    Console.WriteLine("REDUCING---- {0} | chart index: {1} |", NewState, i);
                   
                }
               
            }
            return res;
        }



        //private 
        private ParseState Shift(List<Token> tokens, int i, string x, List<string> ab, List<string> cd, int j,ParseState state)
        {
            if (cd.Count > 0 && tokens[i].Type == cd[0])
            {
                List<string> newAB = new List<string>(ab) {cd[0]};
                List<string> newCD = new List<string>(cd);
                newCD.RemoveAt(0);
                Console.WriteLine("SHIFTING---- " + tokens[i].Value + " (i={0} , j={1})", i, j);

                ParseState res = new ParseState(x, newAB, newCD, j);
                foreach (AstNode s in state.abTree)
                     res.abTree.Add(s);

                if (tokens[i].Value == "(" || tokens[i].Value == ")")
                    return res;

                int num;
                AstNode n;
                if (tokens[i].Type == "STRING")//if it's a string, build the string tree but cut off the quotes.
                    res.abTree.Add(n = new StringNode(tokens[i].Value.Substring(1,tokens[i].Value.Length -2)));
                else if (int.TryParse(tokens[i].Value, out num) || "+-*/==><>=<=||&&".Contains(tokens[i].Value))
                    res.abTree.Add(n = new Number(tokens[i].Value));
                else
                    res.abTree.Add(n = new Identifier(tokens[i].Value));
                
                //if(res.Completed)
                    res.tree.Add(n);//new Number(tokens[i].Value)
                return res;

            }
            return null;
        }

        private List<ParseState> closure(Grammmar grammar, int i, string x, List<string> ab, List<string> cd)
        {
            // x->ab.cd
            var res = from rule in grammar.Rules//!cd.SequenceEqual(new List<object>())
                      where cd.Count > 0 && rule.elements[0].Equals(cd[0])
                      select
                          new List<ParseState>()
                              {new ParseState(rule.elements[0], new List<string>(), (List<string>) rule.elements[1], i)};

            return res.Select(list => list[0]).ToList();
        }



        private bool AddToChart(Dictionary<int, List<ParseState>> chart, int index, ParseState state)
        {
            if (!chart[index].Contains(state))
            {
                chart[index].Add(state);
                return true;
            }
            return false;
        }

        #endregion



        public  AssignmentStatment ParseAssignment(List<Token> tokens)
        {
            string id = tokens[0].Value;
            tokens.RemoveAt(0);
            tokens.RemoveAt(0);
            return new AssignmentStatment(id, ParseExpression(tokens));
        }

        public AstNode ParseExpression (List<Token> tokens)
        {
            //BinaryExpression exp = null;
            AstNode exp = ParseTerm(tokens);
            while (tokens.Count > 0 && (tokens[0].Value == "+" || tokens[0].Value == "-"))
            {
                string op = tokens[0].Value;
                tokens.RemoveAt(0);

                exp = new BinaryExpression(exp, ParseTerm(tokens), op);
            }
            return exp;
        }

        public AstNode ParseTerm(List<Token> tokens)
        {
            AstNode op1 = ParseFactor(tokens);
            tokens.RemoveAt(0);

            BinaryExpression exp = null;
            while (tokens.Count > 0 && (tokens[0].Value == "*" || tokens[0].Value == "/"))
            {
                string op = tokens[0].Value;
                tokens.RemoveAt(0);

                exp = new BinaryExpression(op1,ParseFactor(tokens),op);
                tokens.RemoveAt(0);
            }

            return exp ?? op1;
        }

        public AstNode ParseFactor(List<Token> tokens)
        {
            if (tokens[0].Type == "IDENTIFIER")
                return new Identifier(tokens[0].Value);
            int num = Convert.ToInt32(tokens[0].Value);
            return new Number(num);
        }

    }//class
}
