﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lexer
{
    class Grammmar
    {
        public List<ParseRule> Rules;

        public Grammmar()
        {
            Rules = new List<ParseRule>()
                        {
                            new ParseRule("Program : Statement"),

                            new ParseRule("Statement : Assignment"),
                            new ParseRule("Statement : Expression"),
                            new ParseRule("Statement : BoolExpression"),
                            new ParseRule("Statement : PrintStatement"),

                            new ParseRule("Assignment : IDENTIFIER ASSIGN Expression"),

                            //new ParseRule("PrintStatement : PRINT Expression"),
                          //  new ParseRule("PrintStatement : PRINT BoolExpression"),
                            new ParseRule("PrintStatement : PRINT String"),
                           // new ParseRule("PrintStatement : PRINT STRING Expression"),
                        //    new ParseRule("PrintStatement : PRINT STRING BoolExpression"),
                        new ParseRule("String : String PLUS String"),
                        new ParseRule("String : STRING"),
                            new ParseRule("BoolExpression : BoolExpression AND BoolExpression"),
                            new ParseRule("BoolExpression : BoolExpression OR BoolExpression"),
                            new ParseRule("BoolExpression : Expression Equals Expression"),
                            new ParseRule("BoolExpression : Expression GREATERTHAN Expression"),
                            new ParseRule("BoolExpression : Expression SMALLERTHAN Expression"),
                            new ParseRule("BoolExpression : Expression BIGGEROREQUAL Expression"),
                            new ParseRule("BoolExpression : Expression SMALLEROREQUAL Expression"),
                            new ParseRule("Equals : ASSIGN ASSIGN"),
                           
                            new ParseRule("Expression : Expression PLUS Expression"),
                            new ParseRule("Expression : Expression MINUS Expression"),
                            new ParseRule("Expression : Term"),
                            new ParseRule("Expression : STRING"),
     
                           
                            new ParseRule("Term : Term TIMES Term"),
                            new ParseRule("Term : Term DIVIDE Term"),
                            new ParseRule("Term : NUMBER"),
                            new ParseRule("Term : IDENTIFIER"),
                        };
        }
    }
}
