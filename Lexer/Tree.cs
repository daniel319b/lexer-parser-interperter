﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lexer
{
    public class Tree
    {
        public Node root;

     //   public List<Node> Parse(List<Token> tokens)
       // {
       //     r
      //  }

        //2+2 == 4
        public Node ParseBoolean(List<Token> tokens)
        {
            Node expTree = ParseExpression(tokens);//Parses the left side expression.

            if (tokens[0].Value == "==" || tokens[0].Value == "<" || tokens[0].Value == ">")
            {
                Node op = new Node(tokens[0].Value,"BoolOp");//get the operator.
                tokens.RemoveAt(0);//remove the operator.
                Node rightSide = ParseExpression(tokens);//Parse the right side expression.
                expTree = buildNode(expTree, rightSide, op);
            }
            return expTree;
        }

        public Node ParseStatment(List<Token> tokens)
        {
            Node tree = null;
            //List<Node> trees = new List<Node>();
            while (tokens.Count > 0 && tokens.Contains(new Token("=", "ASSIGN")))
            {
                //x = 2+3
                Node identifier = new Node(tokens[0]); tokens.RemoveAt(0);   
                Node op = new Node(tokens[0].Value,"Assignment"); tokens.RemoveAt(0);
                Node leftSide = ParseExpression(tokens);
                
                tree = buildNode(leftSide, identifier, op);
                //trees.Add(tree);
            }
           return tree;
        }

        public Node ParseExpression(List<Token> tokens)
        { 
            Node temp = null;
            
            Node expTree  = ParseTerm(tokens);

            while (tokens.Count > 0 && 
                (tokens[0].Value == "+" ||
                 tokens[0].Value == "-"))
            {
                Node op = new Node(tokens[0].Value, "BinOp");
                tokens.Remove(tokens[0]);

                temp = ParseTerm(tokens);
                expTree = buildNode(expTree,temp,op);
            }
            return expTree;
        }

        private Node ParseTerm(List<Token> tokens)
        {
            Node temp;

            //A term its either a number by itself or some number with * or / 
            Node expTree = ParseNumber(tokens);//get the first number.
            tokens.Remove(tokens[0]);

            //try to parse a multiplication/division term.
            while (tokens.Count > 0 &&
                (tokens[0].Value == "*" ||
                 tokens[0].Value == "/"))
            {
                Node op = new Node(tokens[0].Value,"Binop");//Get the operator.
                tokens.Remove(tokens[0]);

                temp = ParseNumber(tokens);
                expTree = buildNode(expTree, temp, op);
                tokens.Remove(tokens[0]);
            }
            return expTree;
        }

        private Node buildNode(Node n, Node n2, Node op)
        {
            Node res = new Node(op.Value)
                           {
                               Type = op.Type, 
                               LeftSon = n,
                               RightSon = n2
                           };
            return res;
        }

        public Node ParseNumber(List<Token> tokens)
        {
            Node res = null;
          
            res = new Node(tokens[0]);
            if (tokens[0].Type == "OPENPAREN")
            {
                tokens.Remove (tokens[0]);//remove '('
                res = ParseExpression(tokens);//parse the expression inside. 
            }
            return res;
        }

        public object evalTree(Node t, Dictionary<string, object> environment)
        {
            if (t.Type.Equals("BinOp"))
                return evalExpressionTree(t, environment);
            else if (t.Type.Equals("BoolOp"))
                return evalBool(t, environment);
            else if (t.Type.Equals("Assignment"))
                evalAssignment(t, environment);
            return null;

        }

        public void evalAssignment(Node t, Dictionary<string, object> environment)
        {
            string varName = t.RightSon.Value.ToString();
            object assignmentExpression = evalExpressionTree(t.LeftSon, environment);
            environment[varName] = assignmentExpression;
        }
        public int evalExpressionTree(Node t, Dictionary<string, object> environment)
        {
            if (t.Type.Equals("NUMBER"))//if its a number, just return it.
                return int.Parse(t.Value.ToString());
            else if (t.Type.Equals("IDENTIFIER"))
            {
                if (environment.ContainsKey(t.Value.ToString()))
                    return (int)environment[t.Value.ToString()];
                else
                    throw new Exception("Variable \"" + t.Value.ToString() + "\" not defined");
            }
            //Evaluate each sub tree and then add/subtract/multiply/divide
            //leftTree.value -(BINOP)- rightTree.value
            else switch (t.Value.ToString())
            {
                case "+":
                    return evalExpressionTree(t.LeftSon, environment) + evalExpressionTree(t.RightSon, environment);
                case "-":
                    return evalExpressionTree(t.LeftSon, environment) - evalExpressionTree(t.RightSon, environment);
                case "*":
                    return evalExpressionTree(t.LeftSon, environment) * evalExpressionTree(t.RightSon, environment);
                case "/":
                    return evalExpressionTree(t.LeftSon, environment) / evalExpressionTree(t.RightSon, environment);
            }
            return 0;

        }

        public bool evalBool(Node t, Dictionary<string, object> environment)
        {
            switch (t.Value.ToString())
            {
                case "==":
                    return evalExpressionTree(t.LeftSon,environment) == evalExpressionTree(t.RightSon,environment);
                case ">":
                    return evalExpressionTree(t.LeftSon,environment) > evalExpressionTree(t.RightSon,environment);
                case "<":
                    return evalExpressionTree(t.LeftSon,environment) < evalExpressionTree(t.RightSon,environment);
                default:
                    throw new Exception("Invalid logic operator");
            }
        }
    }

    public class Node
    {
        public object Value { get; set; }
        public string Type { get; set; }
        public Node LeftSon { get; set; }
        public Node RightSon { get; set; }

        public Node(object value)
        {
            Value = value;
        }

        public Node(object value, string type)
        {
            Value = value;
            Type = type;
        }

        public Node(Token t)
        {
            Value = t.Value;
            Type = t.Type;
        }

        public override string ToString()
        {
            return Value.ToString();
        }

        public override bool Equals(object obj)
        {
            Node n = obj as Node;
            if (n != null)
                return n.Value == Value && n.Type == Type;
            return false;
        }
        
    }
}
