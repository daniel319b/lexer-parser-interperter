﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lexer
{
    public class Token
    {
        public string Value { get; set; }
        public string Type { get; set; }

        public Token(string value, string type)
        {
            Value = value;
            Type = type;
        }

        public override string ToString()
        {
            return Value + " - " + Type;
        }

        public override bool Equals(object obj)
        {
            Token other = obj as Token;
            return other != null && (Value == other.Value && Type == other.Type);
        }
    }
}
