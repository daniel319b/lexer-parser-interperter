﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Lexer
{
    public class Lexer
    {

        public List<Token> TokenList { get; set; }
        public string InputString { get; set; }

        public List<Token> tokenize(string s, TokenDefenition[] defenitions)
        {
            InputString = s;
            List<Token> result = new List<Token>();

            int currentIndex = 0;
            TokenDefenition matchedDefenition = null;

            while (currentIndex < s.Length)
            {
                int matchLength = 0;
                foreach (TokenDefenition rule in defenitions)
                {
                    Match m = rule.regex.Match(s, currentIndex);//try to match
                    currentIndex += countSpace(s, currentIndex, currentIndex + m.Length);//count the spaces and advance
                    if (rule.Type == "STRING" && m.Success) currentIndex = m.Index;

                    if (m.Success && (m.Index - currentIndex == 0))//we found a match!
                    {
                        matchLength = m.Length;//get the length of the matched string.
                        matchedDefenition = rule;//get the matched Token Defenition.
                        break;//Exit the loop because if found a match.
                    }
                }
                if (matchedDefenition != null)
                {
                    string tokenValue = s.Substring(currentIndex, matchLength);//get the token.
                    Token token = new Token(tokenValue, matchedDefenition.Type);
                    result.Add(token);
                    if (matchedDefenition.Type == "STRING") currentIndex++;
                }

                currentIndex += matchLength;//cutoff this token.
            }

            TokenList = result;
            return result;
        }

        //tokenize

        static int countSpace(string s, int from, int to)
        {
            int count = 0;
            for (int i = from; i < to; i++)
                if (s[i] == ' ')
                    count++;
            return count;
        }
    }
}
