﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lexer
{
    class ParseState
    {

        public string x;
        public List<string> ab, cd;
        public int j;

        public List<AstNode> tree = new List<AstNode>(), abTree = new List<AstNode>();
        public ParseState(object x, List<string> ab, List<string> cd, int j)
        {
            this.x = x.ToString();
            this.ab = ab;
            this.cd = cd;
            this.j = j;
           // OriginState = new List<ParseState>();
        }
        //State ===   x -> ab.cd ,j


        public override bool Equals(object obj)
        {
            ParseState s = obj as ParseState;
            if (s != null)
            {
                return x.Equals(s.x) && ListsEquals(s) && j == s.j;
            }
            return false;
        }

        private bool ListsEquals(ParseState s)
        {
            //if (ab.Count != s.ab.Count) return false;
            //if (cd.Count != s.cd.Count) return false;
            //for (int i = 0; i < ab.Count; i++)
            //    if (!ab[i].Equals(s.ab[i]))
            //        return false;
            //for (int i = 0; i < cd.Count; i++)
            //    if (!cd[i].Equals(s.cd[i]))
            //        return false;
            //return true;
            return cd.SequenceEqual(s.cd) && ab.SequenceEqual(s.ab);
        }


        public override string ToString()
        {
            string list1 = "", list2 = "";
            foreach (string s in ab)
                list1 += s + " ";
            foreach (string s in cd)
                list2 += s + " ";
            // string stateString = "";
           // foreach (ParseState state in OriginState)
             //   stateString += state + " ";
            return x + " -> " + list1 + ". " + list2 + " from " + j;// +" , FROM STATE: " + OriginState;
        }



        public bool Completed
        {
            get { return cd.Count == 0; }
        }
    }
}
