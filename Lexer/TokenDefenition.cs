﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Lexer
{
    public class TokenDefenition
    {
        public string Type { get; set; }
        public Regex regex { get; set; }

        public TokenDefenition(string type, string regex)
        {
            Type = type;
            this.regex = new Regex(regex);
        }


    }
}
