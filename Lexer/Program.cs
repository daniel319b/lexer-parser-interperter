﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Lexer
{
    class Program
    {
        static void Main(string[] args)
        {

            TokenDefenition[] defenitions =
                {
                    new TokenDefenition("PRINT","print"), 
                    new TokenDefenition("STRING",string.Format("\\{0}[^{1}]*\\{2}",'"','"','"')),
                    new TokenDefenition("IDENTIFIER", "[_?a-zA-Z]+"),
                    //\\"[^\"]*\\"
                    new TokenDefenition("NUMBER", "[0-9]+"),
                    new TokenDefenition("BIGGEROREQUAL", ">="), new TokenDefenition("SMALLEROREQUAL", "<="),
                    new TokenDefenition("GREATERTHAN", ">"), new TokenDefenition("SMALLERTHAN", "<"),               
                    new TokenDefenition("ASSIGN", "="),
                    new TokenDefenition("PLUS", "\\+"), new TokenDefenition("MINUS", "-"),
                    new TokenDefenition("TIMES", "\\*"), new TokenDefenition("DIVIDE", "\\/"),
                    new TokenDefenition("OPENPAREN", "\\("), new TokenDefenition("CLOSEPAREN", "\\)"),
                    new TokenDefenition( "OR","\\|\\|"),new TokenDefenition( "AND","&&"),
                   
                };


           // string stringToTokenize = Console.ReadLine();
            
            Lexer lex = new Lexer();
            List<AstNode> trees = new List<AstNode>();

          //  string stringToTokenize = "x = 1 - 4 + 6*3-6/2-10*3/1;" +
                          //            "y = x*2 + 3;" +
                           //           "z = x * 2 +10 / 2;" +
                            //          "z = z - 10;"+"num = x+1*90*z/y";
            string stringToTokenize = "print \"1+2 is = \" + \"I Think 3\"";//string.Format("print {0}","\"Hello\"");

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            foreach(string s in stringToTokenize.Split(';'))
            {
                List<Token> tokens = lex.tokenize(s, defenitions);
                Console.WriteLine("\nTokens:");
                foreach (Token token in tokens)
                    Console.WriteLine(token.ToString());
                Parser p = new Parser(tokens);
                
                AstNode parseTree = p.Parse();
                trees.Add(parseTree);
            }

            //Console.WriteLine("Parsing:"+stopwatch.Elapsed.TotalSeconds+"\n");
            stopwatch.Restart();


            Dictionary<string, object> env = new Dictionary<string, object>();
            foreach (AstNode tree in trees)
                evalTree(tree,env);
          //  foreach (var key in env)
                //Console.WriteLine("{0} = {1}",key.Key,key.Value);

          //  Console.WriteLine("\nInterperting:" + stopwatch.Elapsed.TotalSeconds + "\n"); stopwatch.Stop(); 
          //  List<Token> tokens = lex.tokenize(stringToTokenize, defenitions);
            

            


            //Console.WriteLine("\n"+parsed);
           // Tree t = new Tree();
           // t.root = t.ParseExpression(tokens);
         //  // List<Node> trees = t.ParseStatment(tokens);
         //   t.root = t.ParseStatment(tokens);
         //   PrintTree(t.root);
         //   Dictionary<string, object> env = new Dictionary<string, object>();

         //   //foreach (Node tree in trees)
         //       //tree.evalTree(tree, env);
         //   Console.WriteLine("\nResult = " + t.evalTree(t.root, env));
         //   t.root = t.ParseExpression(lex.tokenize("a+3", defenitions));
         //   Console.WriteLine("\nResult = " + t.evalTree(t.root, env));
            Console.ReadLine();
        }

        private static object evalTree(AstNode tree, Dictionary<string, object> env)
        {
            if (tree.NodeType == NodeType.Print)
                Console.WriteLine(evalTree(((PrintStatement)tree).ValueTree, env));           
            if (tree.NodeType == NodeType.Assign) 
                evalAssignment(tree, env);
            if (tree is BinaryExpression || tree is Identifier || tree is Number)
                return evalExpressionTree(tree, env);
            if (tree is StringNode)
                return ((StringNode)tree).Value;

            return null;
        }

        private static void evalAssignment(AstNode tree, Dictionary<string, object> env)
        {
            string varName = ((AssignmentStatment) tree).Identifier;
            int assignmentExpression = evalExpressionTree(((AssignmentStatment)tree).Expression, env);
            env[varName] = assignmentExpression;
        }

         private static int evalExpressionTree(AstNode tree, Dictionary<string, object> env)
         {
             switch (tree.NodeType)
             {
                 case NodeType.Number:
                     return Convert.ToInt32(((Number) tree).Value);
                 case NodeType.Identifier:
                     return (int) env[((Identifier) tree).Value];
                 case NodeType.Add:
                     return evalExpressionTree(((BinaryExpression) tree).Operand1, env) +
                            evalExpressionTree(((BinaryExpression) tree).Operand2, env);
                 case NodeType.Subtract:
                     return evalExpressionTree(((BinaryExpression)tree).Operand1, env) -
                            evalExpressionTree(((BinaryExpression)tree).Operand2, env);
                 case NodeType.Multiply:
                     return evalExpressionTree(((BinaryExpression)tree).Operand1, env) *
                            evalExpressionTree(((BinaryExpression)tree).Operand2, env);
                 case NodeType.Divide:
                     return evalExpressionTree(((BinaryExpression)tree).Operand1, env) /
                            evalExpressionTree(((BinaryExpression)tree).Operand2, env);
             }
             return int.MaxValue;
            
         }
    }//class
}
